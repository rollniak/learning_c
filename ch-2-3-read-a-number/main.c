#include <stdio.h>	//library used to import printf and scanf.

int main(void)
{
	float x;	// we initialize the vairable x

	printf("Please enter a real number on the keyboard\n");
	scanf("%f", &x); // keyboard reading of the value of x
	// Display of x
	printf("You typed %f, congratulations!\n", x);
	return 0;
}
