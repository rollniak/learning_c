#include <stdio.h>

int main(void)
{
	float x, y; //declaration of two variables x and y

	printf("please enter a real number on the keyboard\n");
	scanf("%f", &x); //Reading the value of x from the keyboard.
	y = 2*x; //We put in y the double of the content of x.
	printf("The double of the typed number is worth %f \n", y);
	return 0;
}
