/*
 * The purpose of this program is to convert
 * Celsius degrees to Fahrenheit degrees.
*/
#include <stdio.h>

int main(void)
{
	float f, c; //We declare f for Fahrenheit and c for Celsius.

	printf("Enter a value in Celsius:\n");
	
	/*
	 * The value in degrees Fahrenheit is
	 * retrieved by entering the keyboard.
	*/
	scanf("%f", &c);

	// The given value is converted to Celsius. 
	//f = c * 1.8 + 32; My version.
	// The correction of the exercise :
	f = (c / 0.55556) + 32;

	printf("The value %f Clesius in is %f in Fahrenheit.\n", c, f);
	return 0;
}
