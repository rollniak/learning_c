/*
 * The purpose of this program is to convert
 * Fahrenheit degrees to Celsius degrees.
*/
#include <stdio.h>

int main(void)
{
	float f, c; //We declare f for Fahrenheit and c for Celsius.

	printf("Enter a value in Fahrenheit:\n");
	
	/*
	 * The value in degrees Fahrenheit is
	 * retrieved by entering the keyboard.
	*/
	scanf("%f", &f);

	// The given value is converted to Celsius. 
	c = 0.55556 * ( f - 32);

	printf("The value %f in Fahrenheit is %f in Clesius.\n", f, c);
	return 0;
}
