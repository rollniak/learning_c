#include <stdio.h>

int main(void)
{
	float cost, result;
	printf("Enter the price of the item to know its real cost (-10 per cent discount): \n");
	scanf("%f", &cost);
	result = cost - cost * 10 / 100;

	printf("The actual price after the discount of the item is: %f Euro\n", result);
	return 0;
}
